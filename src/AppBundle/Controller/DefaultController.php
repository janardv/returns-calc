<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Nav;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $nav = $em->getRepository(Nav::class)->findAll();
        $SchemeName = Array();
        foreach ($nav as $val) {
            array_push($SchemeName, $val->getSchemeName());
        }
        return $this->render('AppBundle:Nav:index.html.twig', array(
                    'schnumber' => $SchemeName,
        ));
    }

    /**
     * @Route("/show", name="show")
     */
    public function showAction() {
        //$respText = file_get_contents('C:\symfonyProjects\returnsCalc\NAVAll.txt');
        $respText = file_get_contents('https://www.amfiindia.com/spages/NAVAll.txt');
        $respAry = explode(PHP_EOL, $respText);
        $divdAry = Array();
        foreach ($respAry as $resAry) {
            array_push($divdAry, explode(';', $resAry));
        }

        foreach ($divdAry as $divParent) {
            if (sizeof($divParent) > 5) {
                //echo $divParent[3].'<br>';
                if ($divParent[3] == "HDFC Mid Cap Opportunities Fund -Direct Plan - Growth Option") {
                    $invAmt = 25000;
                    $utsbought = 701.8130;
                    echo $divParent[3] . ' - ' . (($divParent[4] * 701.8130) - $invAmt);
                }
            }
        }
        //print_r($divdAry);
        return new Response(' ');
    }

    /**
     * @Route("/insert", name="insert")
     */
    public function insertAction() {
        echo "insert blocked";
        die();
//     	$respText = file_get_contents('https://www.amfiindia.com/spages/NAVAll.txt');
//     	$respAry = explode(PHP_EOL, $respText);
//     	$divdAry = Array();
//     	foreach($respAry as $resAry)
//     	{array_push($divdAry, explode(';',$resAry));}
//    	
//     	foreach($divdAry as $divParent)
//     	{
//     		if(sizeof($divParent)>5)
//     		{
//     			//echo $divParent[3].'<br>';
//     			$entityManager = $this->getDoctrine()->getManager();
//    			
//     			$nav = new Nav();
// 				$nav->setSchemeName($divParent[3]);
// 				$nav->setNavValue($divParent[4]);
//				
// 				$entityManager->persist($nav);
//				
// 				$entityManager->flush();
// 				echo 'Saved new Nav with id '.$nav->getId(). '<br>';
//     		}
//     	}
//     	//print_r($divdAry);
//     	return new Response(' ');
    }

}
