<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="nav")
 */
class Nav {

    /**
     * @ORM\Column(name = "Scheme_ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name = "scheme_name", type="string", length=200)
     */
    private $schemeName;

    /**
     * @ORM\Column(name = "nav_value", type="integer")
     */
    private $navValue;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set schemeName.
     *
     * @param string $schemeName
     *
     * @return Nav
     */
    public function setSchemeName($schemeName) {
        $this->schemeName = $schemeName;

        return $this;
    }

    /**
     * Get schemeName.
     *
     * @return string
     */
    public function getSchemeName() {
        return $this->schemeName;
    }

    /**
     * Set navValue.
     *
     * @param int $navValue
     *
     * @return Nav
     */
    public function setNavValue($navValue) {
        $this->navValue = $navValue;

        return $this;
    }

    /**
     * Get navValue.
     *
     * @return int
     */
    public function getNavValue() {
        return $this->navValue;
    }

}
